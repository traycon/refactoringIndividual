
public class NewReleasePrice extends Price {
	int getPriceCode() {
		return Movie.NEW_RELEASE;
		}
	public double getCargo(int _daysRented) {
		return _daysRented * 3;
	}
	public int getPuntosAlquilerFrecuente(int _daysRented){
		return (_daysRented > 1) ? 2: 1;
	}
}
