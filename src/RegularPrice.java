
public class RegularPrice extends Price {
	int getPriceCode() {
		return Movie.REGULAR;
		}
	public double getCargo(int _daysRented) {
		double cantidad=2;
		if (_daysRented > 2)
			cantidad += (_daysRented - 2) * 1.5;
		return cantidad;
	}
}
