
public class Rental {
	private Movie _movie;
	private int _daysRented;
	public Rental(Movie movie, int daysRented) {
	_movie = movie;
	_daysRented = daysRented;
	}
	public int getDaysRented() {
	return _daysRented;
	}
	public Movie getMovie() {
	return _movie;
	}
	
	public double getCargo() {
		return _movie.getCargo(_daysRented);
	}
	
	public int getPuntosAlquilerFrecuente() {
		return _movie.getPuntosAlquilerFrecuente(_daysRented);
	}
	
}
